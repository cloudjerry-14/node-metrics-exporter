#!/bin/sh

# Ensure directory exists
mkdir -p /mnt/vpath

# Fetch metrics and write to file with timestamp
curl -s http://node-exporter-svc:9100/metrics > /mnt/vpath/metrics_$(date '+%Y-%m-%dT%H-%M-%S').txt && \
echo 'Metrics written successfully' || \
echo 'Failed to fetch metrics'
