# Node Metrics Exporter Setup

This repository contains configurations and scripts to set up a metrics exporter in a Kubernetes environment. The setup includes a CronJob that periodically fetches metrics from a node-exporter service and stores them in a volume.

**Note** : My cluster is setup on an AWS EC2 instance using minikube 

## Components

### 1. Shell Script (`fetch_metrics.sh`)

A shell script is provided to fetch metrics from the node-exporter service and save them to a specified directory with the filename as current timestamp.

### 2. Dockerfile (`Dockerfile`)

An Alpine Linux-based Dockerfile is included to create a Docker image with curl installed. The shell script is copied into the image, made executable wih (chmod + x ), and set to run by default.

**Building the docker image** 

I have created a CI pipeline using Gitlab's CI feature . The script is written in the .gitlab-ci.yml file with a single stage for building , tagging and pushing the docker file.

Utilized the _Dockerhub_  public repository for storing the docker image. The credentials for authenticating with _Dockerhub_ to push image are stored as variables in the Gitlab CICD settings and pulled in runtime.

This public image can be pulled using the below command
```
docker pull cloudjerry14/metrics-exporter:latest
```
### 3. Node Exporter Deployment and Service (`node-exporter.yml`)

I have created a Deployment for deploying the node exporter for collecting the metrics on my k8 cluster and exposed it as a clusterIP.
This service exposes the node metrics at 
```
http://node-exporter-svc:9100/metrics
```

Creating deployment ad service using node-exporter.yml
```
kubectl apply -f node-exporter.yml
```
Check if deployment is created 
```
kubectl get deployments
```
Verify the pods are running
```
kubectl get pods
```
Check if deployment is exposed as a service, ClusterIP
```
kubectl get services
```
### 4. Kubernetes CronJob

A CronJob resource is defined in Kubernetes to run the shell script periodically. It mounts a volume to store the fetched metrics into the worked node which is the minikube node in our case

I have used minikube on EC2 instance for cluster setup, refer Installations.

Deploy the cron job to your Kubernetes cluster
```
kubectl apply -f cronjob.yml
```
Verify the cronjob is created and completed
```
kubectl get cronjob
```

## Validation
After the cronjob completes successfully, you can check if the file is created or not.
you will need to ssh into the worked node, since we are using hostPath as a mount to persist the generated files

As we are using minikube, run the command to ssh into the node
```
minikube ssh
```
change the directory to mounted directory and view the files
```
cd /mnt/vpath/
```
![Files created by cronjob](images/image.png)
## Troubleshooting

**Pod Troubleshooting**
``` 
kubectl describe pod pod_name
 ```
 to check logs
 ```
 kubectl logs pod_name 
```

Exec into pod
```
kubectl exec -it pod_name -- sh
```

## Minikube Installation

Download and install the minikube files
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube && rm minikube-linux-amd64
```

Start the minikube cluster
```
minikube start
```
Alternatively, minikube can download the appropriate version of kubectl and you should be able to use it like this:
```
minikube kubectl -- get po -A
```
You can also make your life easier by adding the following to your shell config: (for more details see: kubectl)
```
alias kubectl="minikube kubectl --"

```
