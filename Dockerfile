# Linux base image
FROM alpine:latest

# Install curl
RUN apk --no-cache add curl

# Copy the script file into the container
COPY fetch_metrics.sh /fetch_metrics.sh

# Make the script executable
RUN chmod +x /fetch_metrics.sh

# Run the script
CMD ["/fetch_metrics.sh"]
